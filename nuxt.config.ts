export default defineNuxtConfig({
  devtools: { enabled: true },
  app: {
    head: {
      title: 'Welcome to thepita Website.',
      htmlAttrs: { lang: 'en' },
      meta: [{ charset: 'utf-8' }, { name: 'viewport', content: 'width=device-width, initial-scale=1' }, { hid: 'description', name: 'description', content: '' }],
      link: [
        { rel: 'icon', type: 'image/x-icon', href: '/assets/icons/favicon.ico' },
        { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css' }
      ]
    }
  },
  typescript: {
    strict: true,
    typeCheck: true
  },
  css: ['@/assets/scss/tailwind.scss'],
  plugins: [],
  components: true,
  build: {},
  modules: [
    ['@nuxtjs/eslint-module', { fix: true }],
    '@nuxt/devtools',
    '@vite-pwa/nuxt',
    '@nuxtjs/tailwindcss',
    '@nuxt/image',
    'nuxt-icons'
  ],
  dir: {
    static: 'static'
  },
  pwa: {
    manifest: {
      name: 'thepita',
      short_name: 'thepita',
      description: 'This is description for PWA apps',
      lang: 'en',
      start_url: '/',
      display: 'standalone',
      background_color: '#FFF6F6',
      theme_color: '#000000',
      icons: [
        {
          src: '/assets/icons/Small-Logo-ThePita.png',
          sizes: '192x192',
          type: 'image/png'
        }
      ]
    },
    devOptions: {
      enabled: true
    }
  }
})
